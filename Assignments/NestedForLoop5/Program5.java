
import java.io.*;

class Demo{

	public static void main(String[]args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter number of rows");
		int row = Integer.parseInt(br.readLine());
		
		int a,b=0,c=1;
		for(int i=1; i<=row; i++) {
			
			for(int j=1; j<=i; j++) {
			
				System.out.print(b+"\t");
				a = b;
				b = c;
				c = a+b;
			}
			System.out.println();
		}
	}
}
