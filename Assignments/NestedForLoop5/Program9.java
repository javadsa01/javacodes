
import java.io.*;
class Demo {

	public static void main(String[]args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter number");
		int num = Integer.parseInt(br.readLine());
		
		int rem;
		int sum=0;
		while(num>0) {

			rem = num%10;
			int mult = 1;
			while(rem>0) {
			
				mult = mult*rem;
				rem--;
			}
			sum = sum+mult;
			num = num/10;
		}
		System.out.println("Sum of factorial of digit is :"+sum);
	}
}
		
