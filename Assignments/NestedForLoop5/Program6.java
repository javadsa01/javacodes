
import java.io.*;

class Demo {

	public static void main(String[]args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter 1st character");
		char ch1 = (char)br.read();
	
		br.skip(1);	
		System.out.println("Enter 2nd character");
		char ch2 = (char)br.read();

		if(ch1==ch2) {
		
			System.out.println(ch1);
		} else if((ch1<91 && ch2<91) || ((ch1>96 && ch1<123) && (ch2>96 && ch2<123))){
			
			if(ch1>ch2) {
			
				System.out.println(ch1-ch2);
			} else {
			
				System.out.println(ch2-ch1);
			}
		} else {
		
			if(ch1>ch2) {
			
				System.out.println(ch1-ch2-6);
			} else {
			
				System.out.println(ch2-ch1-6);
			}
		}
	}
}
