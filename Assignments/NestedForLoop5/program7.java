
import java.io.*;

class Demo {

	public static void main(String[]args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter number of rows");
		int row = Integer.parseInt(br.readLine());

		int num = (row*(row+1))/2;
		char ch = 64;
		ch+=num;
		
		for(int i=1; i<=row; i++) {
		
			for(int j=1; j<=i; j++) {
			
				if(row%2==0) {
				
					if(i%2==0) {
					
						System.out.print(ch+"\t");
					} else {
					
						System.out.print(num+"\t");
					}
				} else {
				
					if(i%2==0) {
					
						System.out.print(num+"\t");
					} else {
					
						System.out.print(ch+"\t");
					}
				}
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
