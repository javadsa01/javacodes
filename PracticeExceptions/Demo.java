class Demo{
	int x = 10;
	int y = 20;
	Demo(){
		System.out.println("In No Args Constructor");
	}
	Demo(int x , int y ){
		this();
		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String [] args){
		Demo obj = new Demo(100,200);
	}
}
