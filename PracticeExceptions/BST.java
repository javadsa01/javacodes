import java.util.*;

public class BinarySearchTree {

    private Node root;

    public BinarySearchTree() {
        root = null;
    }

    // Inserts a new node into the binary search tree.
    public void insert(int value) {
        root = insert(root, value);
    }

    private Node insert(Node node, int value) {
        if (node == null) {
            return new Node(value);
        }

        if (value < node.value) {
            node.left = insert(node.left, value);
        } else if (value > node.value) {
            node.right = insert(node.right, value);
        }

        return node;
    }

    // Finds the number of nodes in the longest path from the root.
    public int getMaxPathLength() {
        return getMaxPathLength(root);
    }

    private int getMaxPathLength(Node node) {
        if (node == null) {
            return 0;
        }

        int leftLength = getMaxPathLength(node.left);
        int rightLength = getMaxPathLength(node.right);

        int maxPathLength = Math.max(leftLength, rightLength) + 1;

        return maxPathLength;
    }

    // Finds the minimum data value found in the tree.
    public int getMinimum() {
        return getMinimum(root);
    }

    private int getMinimum(Node node) {
        if (node == null) {
            return Integer.MAX_VALUE;
        }

        int min = node.value;

        min = Math.min(min, getMinimum(node.left));
        min = Math.min(min, getMinimum(node.right));

        return min;
    }

    // Swaps the roles of the left and right pointers at every node in the tree.
    public void swapLeftAndRightPointers() {
        swapLeftAndRightPointers(root);
    }

    private void swapLeftAndRightPointers(Node node) {
        if (node == null) {
            return;
        }

        Node temp = node.left;
        node.left = node.right;
        node.right = temp;

        swapLeftAndRightPointers(node.left);
        swapLeftAndRightPointers(node.right);
    }

    // Searches for a value in the binary search tree.
    public boolean search(int value) {
        return search(root, value);
    }

    private boolean search(Node node, int value) {
        if (node == null) {
            return false;
        }

        if (value == node.value) {
            return true;
        } else if (value < node.value) {
            return search(node.left, value);
        } else {
            return search(node.right, value);
        }
    }

    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();

        bst.insert(10);
        bst.insert(5);
        bst.insert(15);
        bst.insert(2);
        bst.insert(7);
        bst.insert(12);
        bst.insert(17);

        System.out.println("The number of nodes in the longest path from the root is: " + bst.getMaxPathLength());
        System.out.println("The minimum data value found in the tree is: " + bst.getMinimum());

        bst.swapLeftAndRightPointers();

        System.out.println("The tree after swapping the roles of the left and right pointers is:");
        bst.printTree();

        System.out.println("The value 10 is found in the tree: " + bst.search(10));
        System.out.println("The value 20 is found in the tree: " + bst.search(20));
    }

    private void printTree() {
        if (root == null) {
            return
        }
    }
}
