import java.io.*;
class Demo{
	int min(int arr1[]){
		int min = arr1[0];
		for(int i =0;i<arr1.length;i++){	
		       if (arr1[i]<min){
		       	min = arr1[i];
		       }
         	}return min;
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Size :");
		int size = Integer.parseInt(br.readLine());
		System.out.println("Enter the elements of array :");
		int arr[] = new int [size];
		for (int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		Demo obj= new Demo();
		System.out.println("Minimum Element :"+obj.min(arr));
	}
}
