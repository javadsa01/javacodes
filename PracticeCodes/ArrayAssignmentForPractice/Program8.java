import java.io.*;
class Demo{
	public static void main(String [] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size :");
		int size = Integer.parseInt(br.readLine());
		System.out.println("Enter the elements");
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int narr[] =new int[arr.length];
		int visited =-1;
		for(int i =0;i<arr.length;i++){
			int count = 1;
			for(int j =i+1;i<arr.length;j++){
				if (arr[i]==arr[j]){	
					count++;
					narr[j]=visited;
				}
			}if(narr[i]!=visited){
				narr[i]=count;
			}
			for (int i=0;i<narr.length;i++){
				if(narr[i]!=visited){
					System.out.println("Frequency of element "+arr[i]+"is "+narr[i]);
				}
			}

		}
	}
}

