import java.io.*;
class Demo{
	int max(int arr1[]){
		int max =arr1[0];
		for (int i =0;i<arr1.length;i++){
		       if(arr1[i]>max){
			       max=arr1[i];
		       }
		}return max;
	}

	public static void main(String [] args)throws IOException{
		Demo obj = new Demo();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of an array :");
		int size = Integer.parseInt(br.readLine());
		System.out.println("Enter the elemens an array :");
		int arr[]= new int[size];
	        for (int x =0;x<arr.length;x++){
		     arr[x]=Integer.parseInt(br.readLine());
		}
		System.out.println("Maximum Element :"+obj.max(arr));
	}
}
		
		
