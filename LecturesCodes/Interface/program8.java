interface Demo1{
	void fun();
	void gun();
}
interface Demo2{
	void fun();
	void gun();
}
class Demo implements Demo1,Demo2{
	public void fun(){
		System.out.println("In Demo fun");
	}
	public void gun(){
		System.out.println("In Demo gun");
	}

}
class Client {
	public static void main(String [] args ){
		Demo1 obj = new Demo();
		obj.fun();
		obj.gun();
	}
}

