class Demo{
	Demo(){
		this(10);
		System.out.println("In No-Args Constructor");
	}
	Demo(int x){
		this();
		System.out.println("In Para Constructor");
	}
	public static void main(String [] Shreyas){
		Demo obj = new Demo();
	}
}
