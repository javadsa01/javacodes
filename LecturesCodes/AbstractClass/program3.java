abstract class TodayMatch {
	void MatchInfo(){
		System.out.println("CSK V/s MI");
	}
	abstract void MatchTime();
}
class IPL extends TodayMatch{
	void MatchTime (){
		System.out.println("7:30 pm");
	}
}
class Client {
	public static void main(String [] args ){
		IPL 2023 = new IPL();
		2023.MatchInfo();
		2023.MatchTime();
	}
}

