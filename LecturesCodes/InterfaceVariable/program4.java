interface Demo1{
	int x =10;
}
interface Demo2{
	int x =20;
}
class Demo3 implements Demo1,Demo2{
	int x = 30;
}
class Client{
	public static void main(String[] args){
		Demo3 obj = new Demo3();
		System.out.println(Demo1.x);
		System.out.println(Demo2.x);
	}
}
