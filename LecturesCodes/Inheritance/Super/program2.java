class Parent{
	int x = 10;
	static void fun(){
		System.out.println("In Fun Method");
	}
	void gun(){
		int y = 20;
		System.out.println("In Gun Method");
	}

}
class Child extends Parent{
	public static void main(String [] args){
	Parent obj = new Parent();
	obj.fun();
	obj.gun();
	}
}

