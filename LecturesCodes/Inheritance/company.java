class company{
	company(){
		System.out.println("In Company Class");
		System.out.println("-----------------");
	}
}
class productBased extends company{
	productBased(){
		System.out.println("In Product Class");
	}
}
class Iphone extends productBased{
	Iphone(){
		System.out.println("In Iphone Class");
	}
}
class Client extends Iphone{
	public static void main(String [] Args){
		System.out.println("In Client Class");

		Iphone customer1 =new Iphone(); 
		company customer2 = new Client(); 
		company customer3 = new Client(); 
	}
}


