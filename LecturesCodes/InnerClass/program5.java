class Outer{
	void M1(){
		System.out.println("In-Demo M1");
	}
	class Inner{
		void M2(){
		        System.out.println("In-Inner Class");
		}
	}
}
class Client{
	public static void main(String[] args){
		Outer obj =new Outer();
		Outer.Inner obj1 = obj.new Inner();
		obj.M1();
		obj1.M2();
	}
}
	
