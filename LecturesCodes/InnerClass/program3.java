class Outer {
	void fun(){
		System.out.println("In fun Outer");
	}
	class Inner{
		void fun(){
			System.out.println("In Fun Inner");
		}
	}
	
}
class Clients {
	public static void main(String[] args){
		Outer.Inner obj =new Outer().new Inner();
		obj.fun();
	}
}
