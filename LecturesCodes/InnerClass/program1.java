class outer {
	class inner{
		void m1(){
			System.out.println("In M1");
		}
	}
	void m2(){
		System.out.println("In M2");
	}
}
class Client{
	public static void main(String [] args){
		outer obj =new outer ();
		obj.m2();
		outer.inner obj2 = obj.new inner();
	        obj2.m1();
		outer.inner obj3 = new outer().new inner();
		obj3.m1();
	}
}
