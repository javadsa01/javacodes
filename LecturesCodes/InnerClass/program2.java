class Outer {
	void fun(){
		System.out.println("In Outer fun");
	}
	class Inner{
		void fun(){
			System.out.println("In Inner fun");
		}
	}
	public static void main(String [] args){
		Outer obj = new Outer();
		obj.fun();
	}
	
}
