class Outer1{
	void M1(){
		System.out.println("In-Outer1 ");
	}
	class Inner1{
		void M2(){
			System.out.println("In-Inner1");
		}
	}
}
class Client{
	public static void main(String [] args){
		Outer1 obj = new Outer1();
		Outer1.Inner1= obj.new Inner1();
		obj.M2();
	}
}
