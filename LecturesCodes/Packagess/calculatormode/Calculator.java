package calculatormode;

public class Calculator{

	int num1 = 0;
	int num2 = 0;

	public Calculator(int num1, int num2){
		this.num1= num1;
		this.num2=num2;
	}
	public int add(){
		System.out.print("Addition :");
		return num1+num2;
	}
	public int sub(){
		System.out.print("Subtraction :");
		return num1-num2;
	}
	public int mul(){
		System.out.print("Multiplication :");
		return num1*num2;
	}
	public int div(){
		System.out.print("Division :");
		return num1/num2;
	}

}
