class MyThreads implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class MyThreadGroup {
	public static void main(String[] args){
		ThreadGroup pThreadGP = new ThreadGroup("Shreyas");

		MyThreads obj1 = new MyThreads();
		MyThreads obj2 = new MyThreads();

		Thread t1 = new Thread(pThreadGP,obj1,"Shre");
		Thread t2 = new Thread(pThreadGP,obj2,"yas");


		ThreadGroup cThreadGP = new ThreadGroup(pThreadGP,"Arya");

		MyThreads obj3 = new MyThreads();
		MyThreads obj4 = new MyThreads();

		Thread t3 = new Thread(cThreadGP,obj3,"AR");
		Thread t4 = new Thread(cThreadGP,obj4,"YA");

			ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP,"Shikha");

		MyThreads obj5 = new MyThreads();
		MyThreads obj6 = new MyThreads();

		Thread t5 = new Thread(cThreadGP2,obj5,"Shi");
		Thread t6 = new Thread(cThreadGP2,obj6,"Kha");


		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();


		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());


	}
}
