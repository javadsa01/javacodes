class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());

		for (int i = 0;i<10;i++){
			System.out.println("In MyThread");
		}
	}
}
class ThreadDemo6 {
	public static void main(String[] args)throws InterruptedException{
		MyThread obj = new MyThread();
		System.out.println(Thread.currentThread());
		obj.start();
		
		obj.join();
		for (int i =0 ;i<10;i++){
			System.out.println("In main");
		}
		Thread.currentThread().setName("C2W");
		System.out.println(Thread.currentThread());
	}
}
