class Client extends Thread{
	Client(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
	}
}
class MyThread7 {
	public static void main(String[] args){
		ThreadGroup Game = new ThreadGroup("Cricket");

		Client TeamName1 = new Client(Game,"India");
		Client TeamName2 = new Client(Game,"Pakistan");

		ThreadGroup IPL = new ThreadGroup(Game,"T-20 IPL");

		Client TeamName3 = new Client(IPL,"Mumbai-Indians");
		Client TeamName4 = new Client(IPL,"Chennai Super Kings");

		TeamName1.start();
		TeamName2.start();
		TeamName3.start();
		TeamName4.start();

		System.out.println(Game.activeCount());
		System.out.println(Game.activeGroupCount());
	}
}
