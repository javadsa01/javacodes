class Demo extends Thread{
	Demo(ThreadGroup tg ,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo12{
	public static void main(String[] args){
		ThreadGroup pThread = new ThreadGroup("C2W");
		Demo obj1 = new Demo(pThread,"C");
		Demo obj2 = new Demo(pThread,"C++");
		Demo obj3 = new Demo(pThread,"Python");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}
