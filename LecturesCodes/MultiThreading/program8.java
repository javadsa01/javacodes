class Demo implements Runnable{
	public void run(){
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String [] args){
		Demo obj = new Demo();
		Thread t = new Thread(obj);
		t.start();
		System.out.println(Thread.currentThread().getName());
	}
}

