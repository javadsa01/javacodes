class Demo extends Thread{
	Demo(ThreadGroup th,String str){
		super(th,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class Fun {
	public static void main(String [] args){
		ThreadGroup obj = new ThreadGroup("C@W");

		Demo obj1 = new Demo(obj,"Shreyas");
		Demo obj2 = new Demo(obj,"Arya");
		Demo obj3 = new Demo(obj,"Ranchii");

		obj1.start();
		obj2.start();
		obj3.start();

		System.out.println("________________________________");

		ThreadGroup obj11 = new ThreadGroup("B@M");

		Demo obj4 = new Demo(obj11,"Demo1");
		Demo obj5 = new Demo(obj11,"Demo2");
		Demo obj6 = new Demo(obj11,"Demo3");

		obj4.start();
		obj5.start();
		obj6.start();

	}
}
