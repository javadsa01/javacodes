import java.util.concurrent.*;

class MyThreads implements Runnable{
	int n;
	MyThreads(int n){
		this.n=n;
	}
	public void run(){
		System.out.println(Thread.currentThread()+" Start Thread : "+ n);
		T1();
		System.out.println(Thread.currentThread()+"End Thread : "+n);
	}
	void T1(){
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadPool2{
	public static void main(String[] args){
		ExecutorService scr = Executors.newSingleThreadScheduledExecutor();

		for (int i =1; i<4; i++){
			MyThreads obj = new MyThreads(i);
			scr.execute(obj);
		}
		scr.shutdown();
	}
}

