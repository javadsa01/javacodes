class Demo extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
	Demo (ThreadGroup tg,String str){
		super(tg,str);
	}
}
class ThreadDemo {
	public static void main(String[] args)throws InterruptedException{
		ThreadGroup tg = new ThreadGroup("Company");

		Demo obj1 = new Demo(tg,"ShreyaCreate");
		Demo obj2 = new Demo(tg,"VideoCreate");
		Demo obj3 = new Demo(tg,"CreatorCreate");

		obj1.start();

		obj1.join(1000);
		obj1.yield();

		obj2.start();

		obj2.join(1000);
		obj2.yield();

		obj3.start();

		obj3.join(1000);
		obj3.yield();
	}
}

