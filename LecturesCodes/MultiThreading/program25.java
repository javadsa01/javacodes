import java.util.concurrent.*;

class MyThread implements Runnable{
	int i ;
	MyThread(int i){
		this.i = i;
	}
	public void run(){
		System.out.println(Thread.currentThread()+"Thread start"+i);
		TaskEs();
		System.out.println(Thread.currentThread()+"Thread end"+i);
	}
	void TaskEs(){
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class MyThreadDemoPools{
	public static void main(String[] args){
		ExecutorService ser = Executors.newWorkStealingPool(6);

		for(int i=1;i<5;i++){
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
