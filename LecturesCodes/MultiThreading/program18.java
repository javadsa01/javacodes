class Demo extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
	Demo(ThreadGroup tg ,String str){
		super(tg,str);
	}
}
class ThreadDemo {
	public static void main(String[] args){
		ThreadGroup company = new ThreadGroup("company");


		Demo obj1 = new Demo(company,"Shreyas");
		Demo obj2 = new Demo(company,"Bagal");
		Demo obj3 = new Demo(company,"Arya");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}

