import java.util.concurrent.*;

class MyThreads implements Runnable{
	int num;
	MyThreads(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+"Start Thread : "+num);
		dailyTasks();
		System.out.println(Thread.currentThread()+"End Thread : "+ num);
	}
	void dailyTasks(){
		try{
			Thread.sleep(4000);
		}catch(InterruptedException obj){
			System.out.println(obj.toString());
		}
	}
}
class MyGroupThread {
	public static void main(String [] args){
		ExecutorService ser = Executors.newSingleThreadExecutor();

		for(int i =1;i<=6;i++){
			MyThreads obj = new MyThreads(i);
			ser.execute(obj);
		}ser.shutdown();
	}
}

