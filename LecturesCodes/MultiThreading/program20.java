class Client extends Thread{
	Client(ThreadGroup Mn,String str){
		super(Mn,str);
	}
	public void run(){
		if(Thread.currentThread().isDaemon()){
			System.out.println(getName()+ " is Daemon");
		}else{
			System.out.println(getName()+ " is Not Daemon");
		}
	}
}
class MyThread {
	public static void main(String[] args)throws InterruptedException{
		ThreadGroup Mn = new ThreadGroup("@C2W");

		Client Th = new Client(Mn,"Core");
		Client Th1 = new Client(Mn,"Core1");
		Client Th2 = new Client(Mn,"Core2");

		Th.setDaemon(true);
		Th.start();
		Th1.start();
		Th2.setDaemon(true);
		Th2.start();

		System.out.println(Th.activeCount());
		System.out.println(Mn.activeGroupCount());


	
	}
}
