interface Demo1{
	default void fun(){
		System.out.println("In Demo1");
	}
}
interface Demo2{
	default void fun(){
		System.out.println("In Demo2");
	}
}
class DemoChild implements Demo1,Demo2{
	public void fun(){
		System.out.println("In DemoChild");
	}
}
class Client{
	public static void main(String [] args){
		DemoChild obj = new DemoChild ();
		obj.fun();
	}
}
