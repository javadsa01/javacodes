interface Demo{
	void gun();
	void fun();
}
class DemoChild implements Demo{
	public void gun(){
		System.out.println("In Gun DemoChild");
	}
	public void fun(){
		System.out.println("In Fun DemoChild");
	}
}
class Client {
	public static void main(String [] args){
		DemoChild obj = new DemoChild();
		obj.gun();
		obj.fun();
	}
}
