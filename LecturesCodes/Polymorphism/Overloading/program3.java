class Demo{
	void fun(){
		System.out.println("In Fun Method");
	}
	void fun(int x){
		System.out.println("In Para fun Method");
	}
	void fun(int y,int m){
		System.out.println("In Multi-Para fun method");
	}
	public static void main(String [] args){
		Demo obj = new Demo();
		obj.fun();
		obj.fun(100);
		obj.fun(10,200);
	}
}
