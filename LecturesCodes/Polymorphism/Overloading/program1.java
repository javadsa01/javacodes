class Demo{
	Demo(){
		System.out.println("In No-Args Constructor");
	}
	Demo(int x){
		System.out.println("In Para Constructor");
	}
	Demo(int x, float y){
		System.out.println("In Multi-Para Constructor");
	}
	public static void main(String [] args){
		Demo obj1 = new Demo();
		Demo obj2 = new Demo(10);
		Demo obj3 = new Demo(20,3.498f);
		}
}



