class Demo{
	static void fun(){
		System.out.println("In No-Args Static method()");
	}
	static void fun(int x){
	       System.out.println("In Para Static Method ()");
	}
	static void fun(int x,float y){
		System.out.println("In Multi-Para Static Method()");
	}
	public static void main(String[]args){
		fun();
		fun(10);
		fun(10,3.147f);
	}
}

