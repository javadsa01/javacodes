class Demo{
	void fun(String str){
	}
	void fun(StringBuffer str1){
		System.out.println("In StringBuffer ");
	}
	
}
class Client {
	public static void main(String [] args){
		Demo obj = new Demo();
		obj.fun("Shreyas");
		obj.fun(new StringBuffer("Bagal"));
		obj.fun(null);

	}
}
