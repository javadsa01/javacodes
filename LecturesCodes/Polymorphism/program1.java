class Demo{
	int x = 10;
	Demo(){
		System.out.println("In the Demo Block");
	}
	Demo(int x){
		System.out.println("In the Para Demo Block ");
	}
	Demo(float x){
		System.out.println("In the para Change Block");
	}
	public static void main(String [] args){
		Demo obj = new Demo();
	}
}
