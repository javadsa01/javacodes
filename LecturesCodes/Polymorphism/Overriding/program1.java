class Parent{
	static void fun(){
		System.out.println("In Parent Fun");
	}
	static void gun(){
		System.out.println("In Parent Gun");
	}
}
class Child extends Parent {
	static void gun(){
		System.out.println("In Child Gun");
	}
}
class Client{
	public static void main(String [] args){
		Child.fun();
		Child.gun();
	}
}
