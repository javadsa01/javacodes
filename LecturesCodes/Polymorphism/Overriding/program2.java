class Parent {
	void fun(){
		System.out.println("In Parent Fun");
	}
	void gun(){
		System.out.println("In Parent Gun");
	}
}
class Child extends Parent{
	void gun(){
		System.out.println("In Child Gun");
	}
}
class Client{
	public static void main(String[] args){
		Child obj = new Child();
		obj.fun();
		obj.gun();
	}
}
