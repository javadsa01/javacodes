class Demo{
	void M1(){
		System.out.println("In M1");
	}
	void M2(){
		System.out.println("In M2");
		System.out.println(10/0);
	}
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.M1();
		obj = null;
		try{
			obj.M2();
		}
		catch(ArithmeticException obj1){
			System.out.println(obj1);
			obj.M2();
		}
	}
}
