class player{
	private int jerNo = 0;
	private String name = null;

	player(int jerNo,String name){
		this.jerNo=jerNo;
		this.name=name;
	}
	void info(){
		System.out.println(jerNo+" = "+name);
	}
}
class Game{
	public static void main(String [] args){
		player p1 = new player(18,"virat");
		p1.info();
		player p2 = new player(7,"MSD");
		p2.info();
		player p3 = new player(45,"Rohit");
		p3.info();
	}
}
